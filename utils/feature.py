import numpy as np 
from sklearn.neighbors import NearestNeighbors
from utils.draw_line import plotLine
from utils.matching import cal_similarity
import constant

def cal_dif_angle(angle1, angle2):
    diff = angle1 - angle2
    if (diff > np.pi):
        return 2*np.pi - diff
    elif (diff <= np.pi): 
        return 2*np.pi + diff

    return diff

def cal_distance(p0, p1):
    x0 = p0[0]
    y0 = p0[1]
    x1 = p1[0]
    y1 = p1[1]
    
    return np.sqrt((x1 - x0) ** 2 + (y1 - y0) ** 2)


def get_ridge_count(minutiae_coords, img):

    image = np.uint8(img)
    rows ,cols = img.shape
    coords = np.array([[int(i), int(j)] for i, j, t in minutiae_coords])    
    nbrs = NearestNeighbors(n_neighbors=3, algorithm="ball_tree").fit(coords)
    distances, indices = nbrs.kneighbors(coords)
    ridgecount = []

    for i in range(len(coords)):
        p1 = coords[indices[i][1]]
        p2 = coords[indices[i][2]]
        p1_type = minutiae_coords[indices[i][1]][2]
        p2_type = minutiae_coords[indices[i][2]][2]
        p = coords[i]

        #draw line between p, p1 and p, p2 with coords and pixel value
        line1 = plotLine(p, p1, img)
        line2 = plotLine(p, p2, img)

        
        l1 = [image[int(x), int(y)] for x, y, z in line1]
        
        #drop ridges attach with p and p1
        for j in range(len(l1)):
            if l1[j]==0:
                l1 = l1[j:]
                break

        while l1 and l1[-1]==1:
            l1.pop()
        # count ridges between p and p1
        flag = 1
        line1_ridges = 0
        for j in range(len(l1) - 1):
            if(l1[j] == 0):
                flag = 0
            else:
                if(flag == 0):
                    line1_ridges += 1
                
                flag = 1

        l2 = [image[int(x), int(y)] for x, y, z in line2]

        #drop ridges attach with p and p2
        for j in range(len(l2)):
            if l2[j]==0:
                l2 = l2[j:]
                break

        while l2 and l2[-1]==1:
            l2.pop()        

        # count ridges between p and p2
        flag = 1
        line2_ridges = 0
        for j in range(len(l2) - 1):
            if(l2[j] == 0):
                flag = 0
            else:
                if(flag == 0):
                    line2_ridges += 1
                
                flag = 1        
        
        ridgecount.append([p1, p1_type, line1_ridges, p2, p2_type, line2_ridges])
    
    return ridgecount
        
def getFeatureVector(minutiae_coords, nbrs_coords, orientations):
    features = []

    for i, point in enumerate(minutiae_coords):
        vec = 0        
        p1 = nbrs_coords[i][0]
        p2 = nbrs_coords[i][3]
        p1_type = nbrs_coords[i][1]
        p2_type =nbrs_coords[i][4]
        
        n1 = nbrs_coords[i][2]
        n2 = nbrs_coords[i][5]
        p1_theta = orientations[p1[0]][p1[1]]
        p2_theta = orientations[p2[0]][p2[1]]
        p_theta = orientations[point[0]][point[1]]
        p = (point[0], point[1])
        p_type = point[2]
        
        ft = makeFeatureVector(p,p1, p2, p_theta, p1_theta, p2_theta, p_type, p1_type, p2_type, n1, n2)

        features.append(ft)

    return features


def makeFeatureVector(p, p1, p2, p_theta, p1_theta, p2_theta, p_type, p1_type, p2_type, n1, n2):

    dki = cal_distance(p, p1)
    dkj = cal_distance(p, p2)

    phi_ki = cal_dif_angle(p_theta, p1_theta)
    phi_kj = cal_dif_angle(p_theta, p2_theta)

    tan_di = np.arctan2(p1[1] - p[1], p1[0] - p[0])
    tan_dj = np.arctan2(p2[1] - p[1], p2[0] - p[0])
    theta_ki = cal_dif_angle(tan_di, p_theta)
    theta_kj = cal_dif_angle(tan_dj, p_theta)

    return [p[0], p[1], p_theta, dki, dkj, theta_ki, theta_kj, phi_ki, phi_kj, n1, n2, p_type, p1_type, p2_type]


def cal_polar(fv1, fv2):

    coord_p1 = (fv1[0], fv1[1])
    coord_p2 = (fv2[0], fv2[1])
    theta_p1 = fv1[2]
    theta_p2 = fv2[2]
    diff = (coord_p1[1] - coord_p2[1], coord_p1[0] - coord_p2[0])

    distance = cal_distance(coord_p1, coord_p2)
    theta = cal_dif_angle(theta_p2, theta_p1)
    phi = cal_dif_angle(np.arctan2(diff[1], diff[0]), theta_p1)

    return [distance, theta, phi]

def getPolarFV(fv_arr, index):

    base = fv_arr[index]
    polar_fv = []
    for i in fv_arr:        
        polar = cal_polar(base, i)
        polar_fv.append(polar)

    return np.array(polar_fv)
