# from subprocess import Popen, PIPE

# img = ['101_5.tif', '101_6.tif', '101_7.tif', '101_8.tif', 
#         '102_5.tif', '102_6.tif', '102_7.tif', '102_8.tif',
#         '103_5.tif', '103_6.tif', '103_7.tif', '103_8.tif',
#         '104_5.tif', '104_6.tif', '104_7.tif', '104_8.tif',        
#         '105_5.tif', '105_6.tif', '105_7.tif', '105_8.tif',        
# ]

# res = ""
# f = open("result.txt", "a")
# for i in img: 
#     p = Popen(['python', 'main.py', i], stdout=PIPE, stderr=PIPE, stdin=PIPE)

#     output = p.stdout.read().decode("utf-8")

#     f.write(output)

# f.close()
from skimage.morphology import skeletonize
import skimage
import cv2
import numpy as np
import constant 

from enhance import pre_process
from scipy import signal,ndimage
from utils import extracting

img = cv2.imread("./img/101_2.tif", 0)
fv = extracting.get_descriptor(img)
print(fv)