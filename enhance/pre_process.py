
from enhance.segment import segment
from enhance.orientation import orientation
from enhance.frequency import frequency
from enhance.gabor_filter import gabor_filter

import cv2
import numpy as np
def preprocess(img):

	# Normalize and Segment
	norm_img, mask = segment(img)
	# Get Orientations
	orim = orientation(norm_img)
	
	# Get Frequencies
	freq = frequency(norm_img, mask, orim)	
	
	# Gabor Filter
	newim = gabor_filter(norm_img, orim, freq, mask)

	return (newim<-3), orim, mask