import numpy as np
import cv2

def orientation(img, w = 16):
    rows, cols = img.shape

    #Lọc ảnh và tính toán Sobel
    img = cv2.GaussianBlur(img, (5, 5), 0)
    
    imgX = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=5)
    imgY = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=5)

    xblocks, yblocks = rows // w, cols // w

    # Tính Fx và Fy
    orien = np.zeros((xblocks, yblocks))
    for i in range(xblocks):
        for j in range(yblocks):
            fx, fy = 0, 0

            for x in range(w):
                for y in range(w):
                    fx += imgX[i*w + x, j*w + y] * imgY[i*w + x, j*w + y] * 2
                    fy += imgX[i*w + x, j*w + y] ** 2 - imgY[i*w + x, j*w + y] ** 2

            orien[i, j] = np.arctan2(fx, fy) / 2
    

    orien = orien + np.pi/2

    #Lọc ảnh theo low pass filter 3x3

    smooth_orien = np.empty((xblocks, yblocks))
    orien_img = np.ones(img.shape)
    
    orien = np.pad(orien, 1, mode = "edge")

    for x in range(1, xblocks+1):
        for y in range(1, yblocks+1):
            block = orien[x-1: x+2, y-1: y+2]

            sin_angles = np.mean(np.sin(block * 2))
            cos_angles = np.mean(np.cos(block * 2))
            smooth_orien[x-1, y-1] = np.arctan2(sin_angles, cos_angles) / 2

    for x in range(xblocks):
        for y in range(yblocks):
            orien_img[x*w: (x+1)*w, y*w:(y+1)*w] = smooth_orien[x, y]
    
    # drawOrientation(smooth_orien)

    return orien_img