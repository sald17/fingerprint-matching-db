import numpy as np 
import cv2

#xóa spur bằng cách lọc

def removeSpurByFilter(coords, image, segmentfilter):
    rows, cols = image.shape
    mask = np.zeros((rows, cols))
    coords_no_corner = []
    wins = 41
    left, right = wins//2, wins//2 + 1
    for i, j, t in coords: 
        if i > left and i < rows - right and j > left and j < cols - right:
            block = segmentfilter[i-left: i+right][:,j-left:j+right]
            if(np.sum(block) == wins*wins):
                coords_no_corner.append((i, j, t))                
    coords_no_areas = []
    bada = 7
    for i, j, t in coords_no_corner:
        flag = 0
        for x, y, z in coords_no_corner:
            if(i != x or y != j):                
                if x < i+bada and x > i-bada:
                    if y < j+bada and y > j-bada:
                        flag = 1
                        break
        if(flag == 1):  continue
        coords_no_areas.append((i, j, t))
        mask[i][j] = 1    
    
    if(len(coords_no_areas) > 0):
        return coords_no_areas, mask

    return coords, mask

    


