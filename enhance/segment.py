import numpy as np


def normalize_std(img):
    std = np.std(img)
    mean = np.mean(img)

    return (img - mean) / std


def segment(img, w= 16, thresh = 0.1):
    rows, cols = img.shape
    
    img = normalize_std(img)

    new_rows, new_cols = int(w*np.ceil(rows*1.0/w)), int(w*np.ceil(cols*1.0/w))
    xblocks, yblocks = new_rows//w, new_cols//w

    padded_img = np.zeros((w*xblocks, w*yblocks))
    std_img = np.zeros((new_rows, new_cols))

    padded_img = img
    
    for i in range(xblocks):
        for j in range(yblocks):
            block = padded_img[ i*w:(i+1)*w, j*w:(j+1) * w]

            std_img[i*w:(i+1)*w, j*w:(j+1) * w] = np.std(block)

    std_img = std_img[0:rows, 0:cols]
    mask = std_img > thresh

    mean_val = np.mean(img[mask])
    std_val = np.std(img[mask])

    normim = (img - mean_val) / std_val

    return normim, mask