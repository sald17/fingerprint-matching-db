
from enhance.pre_process import preprocess
import numpy as np

from skimage.morphology import skeletonize
from utils.getMinutae import getMinutae, getMinutaeByCN
from utils.removeSpur import removeSpurByFilter
from utils.feature import get_ridge_count, getFeatureVector
from utils.matching import get_most_similar, match_level
import constant
import cv2

import skimage

def get_descriptor(img):
    enhanced_img, orient_image, segment_filter = preprocess(img)

    # cv2.imshow("enhanced", np.uint8(enhanced_img) * 255)
    image = np.uint8(enhanced_img)*255
    # cv2.imshow("image", image)
    # cv2.waitKey(0)

    #Tạo ảnh thinning
    skeleton_img = skeletonize(enhanced_img)
    skeleton_img = np.uint8(skeleton_img) * 255    

    #Trích xuất
    coords, spoted = getMinutaeByCN(skeleton_img)
    # print(coords)
    #Loại bỏ nhiễu
    coords_af, mask_minutiae = removeSpurByFilter(coords, skeleton_img, segment_filter)
    # ==========================================================================
    
#     rows, cols = skeleton_img.shape
#     cv2.imshow("SKE", skeleton_img)
#     DispImg1 = np.zeros((rows,cols,3), np.uint8)
#     DispImg1[:,:,0] = skeleton_img; 
#     DispImg1[:,:,1] = skeleton_img; 
#     DispImg1[:,:,2] = skeleton_img;
    
#     for x,y,t in coords:
#          (rr, cc) = skimage.draw.circle_perimeter(x, y, 6);
#          skimage.draw.set_color(DispImg1, (rr,cc), (0, 255, 0))
#     cv2.imshow("SPOTED", DispImg1)
#     DispImg = np.zeros((rows,cols,3), np.uint8)
#     DispImg[:,:,0] = skeleton_img; 
#     DispImg[:,:,1] = skeleton_img; 
#     DispImg[:,:,2] = skeleton_img;
    
#     for x,y,t in coords_af:
#          (rr, cc) = skimage.draw.circle_perimeter(x, y, 6);
#          skimage.draw.set_color(DispImg, (rr,cc), (0, 255, 0))
#     cv2.imshow("REMOVED", DispImg)

#     cv2.waitKey(0)

    # cv2.imwrite("spoted.png", DispImg1)
    # cv2.imwrite("removed.png", DispImg) 

    # ================================================================
    ridge_count = get_ridge_count(coords_af, enhanced_img)

    fv = getFeatureVector(coords_af, ridge_count, orient_image)

    return fv

