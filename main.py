import sys
import cv2
import numpy as np
import constant

from utils.feature import getPolarFV
from utils.matching import get_most_similar, match_level
from utils.extracting import get_descriptor

from utils.db_utils import get_feature_array, getListImage

from db_setup import image_colection

def check_fingerprint(input_fv):
    match = -1
    name = ""
    count = 0
    img_list = getListImage()
    for x in img_list:
        fv = get_feature_array(x["feature_vectors"])

        best1, best2, best_value = get_most_similar(input_fv, fv)
        input_polar = getPolarFV(input_fv, best1)
        polar = getPolarFV(fv, best2)
        match_val = match_level(input_polar, polar, input_fv, fv)
        if(match < match_val):
            match = match_val
            name = x["label"]["label"]
            if(match_val == 1): break
        
        count += 1
    return name, match


if __name__ == "__main__":
    len_argv = len(sys.argv)
    if(len_argv == 2):
        img_file = sys.argv[1]
        
        input_img = cv2.imread("./img/" + img_file, 0)
        input_fv = get_descriptor(input_img)    
        best, match_val = check_fingerprint(input_fv)
        print("%s -- Best match from: %s - Value: %s" % (img_file, best, match_val))
    elif(len_argv == 3):
        img1 = sys.argv[1]
        img2 = sys.argv[2]

        img1 = cv2.imread("./img/" + img1, 0)
        img2 = cv2.imread("./img/" + img2, 0)

        fv1 = get_descriptor(img1)
        fv2 = get_descriptor(img2)

        best1, best2, best_value = get_most_similar(fv1, fv2)

        polar1 = getPolarFV(fv1, best1)
        polar2 = getPolarFV(fv2, best2)
        match_val = match_level(polar1, polar2, fv1, fv2)

        print("MATCH VAL: ", match_val)
        isMatch = match_val > constant.matching_thresh
        print("2 fingerprint is matched: ", isMatch)
