import cv2
from utils.extracting import get_descriptor
from db_setup import image_colection


def get_feature_obj(feat_vectors):
    res = []
    for feat in feat_vectors:        
        s = ' '.join(str(x) for x in feat)
        res.append(s)    
    return res

def get_feature_array(feat_obj):
    res = []
    for feat in feat_obj:
        arr = (feat.split(' '))                
        arr =list(map(lambda x: float(x), arr))
        res.append(arr)
    return res

def get_image_obj(file_name, label):
    path = "./img/" +file_name
    img = cv2.imread(path, 0)
    feature_vec = get_descriptor(img)        
    feat_obj = get_feature_obj(feature_vec)    
    
    img_obj = {
        "file_name": file_name,
        "height": img.shape[0],
        "width": img.shape[1],
        "feature_vectors": feat_obj,
        "people": label
    }
    return img_obj


def getListImage():    
    return list(image_colection.aggregate([
        {
            "$lookup": {
                "from": "people",
                "as": "label",
                "localField": "people",
                "foreignField": "_id"
            }
        },
        {
            "$unwind": "$label"
        }
    ]));